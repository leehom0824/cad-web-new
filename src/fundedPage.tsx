import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import FundedSearch from "./component/fundedSearch";
import FundedOverview from "./component/fundedOverview";
import FundedInvestor from "./component/fundedInvestor";
import FundedOtherDeal from "./component/fundedOtherDeal";
import { useFindFinancingOverviewQuery } from "./generated/graphql";
import { useStyles } from "./component/componentStyle"
import Base64 from "base-64";

interface FundedPageProps {
  match: {
    params: {
      companyID: string,
      fundingRoundType: string,
      fundedAt: string,
      raisedAmountUsd: number,
    };
  };
}

export default function FundedPage(props: FundedPageProps) {
  const classes = useStyles();
  const [companyName, setCompanyName] = useState("企查查")
  const [findFinancingOverviewQueryResult] = useFindFinancingOverviewQuery({
    variables: {
      id: Base64.decode(props.match.params.companyID),
      type: props.match.params.fundingRoundType,
      at: props.match.params.fundedAt,
      usd: props.match.params.raisedAmountUsd,
    },
  });
  useEffect(() => {
    let res = findFinancingOverviewQueryResult.data?.QueryFinancingOverviewByFinancingIndex;
    setCompanyName(res?.companyInfo?.companyName as string);
  }, [findFinancingOverviewQueryResult])

  return (
    <div className={classes.root}>
      <Grid container>
        <FundedSearch />
        <Grid item xs={12} className={classes.companyName}>
          {companyName}, {props.match.params.fundingRoundType}, {props.match.params.fundedAt}
        </Grid>
        <Grid item xs={12}>
          <FundedOverview
            id={props.match.params.companyID}
            type={props.match.params.fundingRoundType}
            at={props.match.params.fundedAt}
            usd={props.match.params.raisedAmountUsd}
          />
        </Grid>
        <Grid item xs={12}>
          <FundedInvestor
            id={props.match.params.companyID}
            type={props.match.params.fundingRoundType}
            at={props.match.params.fundedAt}
            usd={props.match.params.raisedAmountUsd}
          />
        </Grid>
        <Grid item xs={12}>
          <FundedOtherDeal id={props.match.params.companyID} />
        </Grid>
      </Grid>
    </div>
  );
}
