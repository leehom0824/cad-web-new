import React from "react";
import "./App.css";
import { createClient, Provider } from "urql";
import { BrowserRouter as Router, Route } from "react-router-dom";
import SearchPage from "./searchPage";
import CompanyPage from "./companyPage";
import FundedPage from "./fundedPage";

// 声明 client 实例
const client = createClient({
  url: "http://127.0.0.1:8080/query",
});

function App() {
  return (
    <Provider value={client}>
      <Router>
        <div>
          <Route exact path="/" component={SearchPage} />
          <Route path="/searchPage" component={SearchPage} />
          <Route path="/companyPage/:id" component={CompanyPage} />
          <Route path="/fundedPage/:companyID/:fundingRoundType/:fundedAt/:raisedAmountUsd" component={FundedPage} />
        </div>
      </Router>
    </Provider>
  );
}
export default App;
