import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        container_search: {
            height: "64px",
            paddingTop: "20px",
            paddingLeft: "32px",
        },
        search: {
            height: "54px",
        },
        companyName: {
            height: "58px",
            fontSize: "20px",
            lineHeight: "58px",
            color: "#333",
            paddingLeft: "32px",
            paddingTop: "4px",
          },
        container_overview: {
            height: "200px",
            paddingTop: "20px",
            paddingLeft: "24px",
            paddingRight: "24px",
            width: "100%",
            background: "#F8F8F8",
        },

        container_mergeinfo: {
            background: "#F8F8F8",
            height: "200px",
            paddingLeft: "24px",
            paddingRight: "24px",
            width: "100%",
        },

        overview: {
            width: "100%",
            height: "100%",
            background: "#fff",
        },

        financing: {
            width: "100%",
            height: "100%",
            background: "#fff",
            paddingLeft: "24px",
            paddingRight: "24px",
        },

        acquisition: {
            width: "100%",
            height: "100%",
            background: "#fff",
            paddingLeft: "24px",
            paddingRight: "24px",
        },

        investment: {
            width: "100%",
            height: "100%",
            background: "#fff",
            paddingLeft: "24px",
            paddingRight: "24px",
        },

        financing_title: {
            fontFamily: "Noto Sans SC Medium",
            fontWeight: "bolder",
            height: "24px",
            paddingTop: "16px",
            paddingBottom: "16px",
            width: "100%",
            fontSize: "16px",
            color: "#333",
        },

        financing_total_amount: {
            fontFamily: "Noto Sans SC Medium",
            fontWeight: "bolder",
            height: "40px",
            width: "100%",
            fontSize: "16px",
            color: "#333",
        },

        overview_title: {
            fontFamily: "Noto Sans SC Medium",
            height: "24px",
            paddingTop: "16px",
            paddingBottom: "16px",
            paddingLeft: "24px",
            width: "100%",
            fontSize: "16px",
            color: "#333",
        },

        overview_info: {
            width: "100%",
            height: "124px",
        },
        merge_info: {
            width: "100%",
            height: "100px",
        },

        infoleft: {
            paddingLeft: "24px",
            width: "397px",
            height: "108px",
            fontSize: "14px",
        },
        infotext: {
            marginTop: "4px",
            height: "24px",
            fontFamily: "Noto Sans SC Medium",
        },

        info_left1: {
            width: "120px",
            color: "#666",

        },
        info_left2: {
            width: "245px",
            color: "#222",
        },
        inforight: {
            height: "108px",
            paddingLeft: "26px",
            fontSize: "14px",
            borderLeft: "1px solid #E8E8E8",
        },

        info_right1: {
            width: "127px",
            color: "#666",

        },
        info_right2: {
            width: "250px",
        },

        container_financing: {
            paddingTop: "20px",
            paddingLeft: "24px",
            paddingRight: "24px",
            width: "100%",
            background: "#F8F8F8",
        },

        container_investment: {
            paddingTop: "20px",
            paddingLeft: "24px",
            paddingRight: "24px",
            width: "100%",
            background: "#F8F8F8",
        },
        container_acquisition: {
            paddingTop: "20px",
            paddingLeft: "24px",
            paddingRight: "24px",
            width: "100%",
            background: "#F8F8F8",
        },

        amount1: {
            marginRight: "16px",
            color: "#737373",
            fontSize: "16px",
        },
        amount2: {
            color: "#333",
        },

        table: {
            minWidth: 650,
        },

        financing_type_style: {
            width: "144px",
        },

        funded_at_style: {
            width: "125px",
        },

        total_amount_style: {
            width: "138px",
        },
        investor: {
            width: "483px",
        },
        investment_founded_at_style: {
            width: "177px",
        },

        investment_object_style: {
            width: "308px",
        },

        company_category_style: {
            width: "221px",
        },

        investment_financing_type_style: {
            width: "220px",
        },

        pagination: {
            float: "right",
        },
        company_logo: {
            width: "110px",
        },
        company_search: {

        },
        search_input: {
            width: "520px",
            height: "36px",
            borderRadius: "18px",
            border: "1px solid #E8E8E8",
            marginLeft: "104px",
            outline: "none",
            paddingLeft: "40px",

        },
        logo_img: {
            paddingTop: "9px",
        },

        merge_none: {
            display: "none",
        },

        noDisplay: {
            display: "none",
        },

        cadAppBar: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
            height: 123,
          },

          companyOverview: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
            height: 180,
          },

          fundedInfo: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
            height: 725,
          },

          investInfo: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
            height: 725,
          },

          acquisitionInfo: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
            height: 725,
          },

          searchIcon: {
            padding: theme.spacing(0, 2),
            height: "70px",
            position: "absolute",
            pointerEvents: "none",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: "grey",
          },

          searchList: {
            height: "36px",
            outline: "none",
            boxShadow: "none",
            color: "#222",
            margin: 0,
            fontWeight: 400,
          },

          input_search: {
            height: "37px",
            borderRadius: "50px 50px 50px 50px",
            outline: "none",
            boxShadow: "none",
            border: "2px solid #c4c7ce",
            background: "#fff",
            color: "#222",
            fontWeight: 400,
          },

          img_logo: {
            display: "block",
            paddingTop: "23px",
            paddingBottom: "23.4px",
            marginLeft: "24px",
          },

          container_searchs: {
            height: "64px",
          },
    })

);