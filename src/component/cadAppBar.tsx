import React from "react";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import { useStyles } from "./componentStyle"
import logo from './logo.png';

interface CadAppBarProps {
    id: string
}

export default function CadAppBar(props: CadAppBarProps) {
    const classes = useStyles();
    return (
        <div>
            <Grid item xs={12} className={classes.container_search}>
                <Box display="flex" flexDirection="row">
                    <Box className={classes.company_logo}>
                        <img className={classes.logo_img} src={logo} alt="" />
                    </Box>
                    <Box className={classes.company_search}>
                        <input className={classes.search_input} placeholder="搜索企业"></input>
                    </Box>
                </Box>
            </Grid>
            <Grid item xs={12} className={classes.companyName}>企查查</Grid>
        </div>
    )
}