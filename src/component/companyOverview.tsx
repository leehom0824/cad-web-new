import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import { useFindCompanyOverviewQuery } from "../generated/graphql";
import Base64 from "base-64";
import LinktoFunded from "./LinktoFunded";
import { useStyles } from "./componentStyle"

interface CompanyOverviewProps {
  id: string;
}

interface FundedIndex {
  companyID: string;
  fundingRoundType: string;
  fundedAt: string;
  raisedAmountUsd: number;
}

export default function CompanyOverview(props: CompanyOverviewProps) {
  const classes = useStyles();
  const [city, setCity] = useState("");
  const [foundingRounds, setFoundingRounds] = useState(0);
  const [foundedAt, setFoundedAt] = useState("");
  const [fundingTotalUSD, setFundingTotalUSD] = useState(0);
  const [statusCode, setStatusCode] = useState("");
  const [firstFunded, setFirstFunded] = useState<FundedIndex>();
  const [lastFunded, setLastFunded] = useState<FundedIndex>();
  const [findCompanyOverviewQueryResult] = useFindCompanyOverviewQuery({
    variables: {
      id: Base64.decode(props.id),
    },
  });

  useEffect(() => {
    let res =
      findCompanyOverviewQueryResult.data?.QueryCompanyOverviewByCompanyID;
    setCity(res?.city as string);
    setFoundingRounds(res?.foundingRounds);
    setFoundedAt(res?.foundedAt as string);
    setFundingTotalUSD(res?.fundingTotalUSD as number);
    setStatusCode(res?.statusCode as string);
    setFirstFunded(res?.firstFunded as FundedIndex);
    setLastFunded(res?.lastFunded as FundedIndex);
  }, [findCompanyOverviewQueryResult]);

  function tranNumber(num: number | undefined, point: number) {
    if (typeof num === "undefined" || num === 0) {
      return "-"
    }
    let n = num as number + ""
    let numStr = n.toString().split('.')[0]
    if (numStr.length < 5) {
      return numStr;
    } else if (numStr.length >= 5 && numStr.length <= 8) {
      let decimal = numStr.substring(numStr.length - 4, numStr.length - 4 + point)
      return Math.floor(num / 10000) + '.' + decimal + '万'
    } else if (numStr.length > 8) {
      let decimal = numStr.substring(numStr.length - 8, numStr.length - 8 + point);
      return Math.floor(num / 100000000) + '.' + decimal + '亿'
    }
  }

  return (
    <div>
      <Grid item xs={12} className={classes.container_overview}>
        <Grid item xs={12} className={classes.overview}>
          <Box className={classes.overview_title}>概览</Box>
          <Box
            className={classes.overview_info}
            display="flex"
            flexDirection="row"
          >
            <Box className={classes.infoleft} display="flex" flexDirection="row">
              <Box className={classes.info_left1}>
                <Box className={classes.infotext}>总部地区</Box>
                <Box className={classes.infotext}>成立日期</Box>
                <Box className={classes.infotext}>经营状态</Box>
              </Box>
              <Box className={classes.info_left2}>
                <Box className={classes.infotext}>{city == null ? '---' : city}</Box>
                <Box className={classes.infotext}>{foundedAt == null ? '---' : foundedAt}</Box>
                <Box className={classes.infotext}>{statusCode == null ? '---' : statusCode}</Box>
              </Box>
            </Box>
            <Box className={classes.inforight} display="flex" flexDirection="row">
              <Box className={classes.info_right1}>
                <Box className={classes.infotext}>融资轮数</Box>
                <Box className={classes.infotext}>首次融资日期</Box>
                <Box className={classes.infotext}>最近融资日期</Box>
                <Box className={classes.infotext}>累计融资额</Box>
              </Box>
              <Box className={classes.info_right2}>
                <Box className={classes.infotext}>{foundingRounds == null ? '---' : foundingRounds}</Box>
                <Box className={classes.infotext}>
                  <LinktoFunded
                    companyID={Base64.encode(firstFunded?.companyID as string)}
                    fundingRoundType={firstFunded?.fundingRoundType as string}
                    fundedAt={firstFunded?.fundedAt as string}
                    raisedAmountUsd={firstFunded?.raisedAmountUsd as number}
                  ></LinktoFunded>
                </Box>
                <Box className={classes.infotext}><LinktoFunded
                  companyID={Base64.encode(lastFunded?.companyID as string)}
                  fundingRoundType={lastFunded?.fundingRoundType as string}
                  fundedAt={lastFunded?.fundedAt as string}
                  raisedAmountUsd={lastFunded?.raisedAmountUsd as number}
                ></LinktoFunded></Box>
                <Box className={classes.infotext}>{tranNumber(fundingTotalUSD, 2) == null ? '---' : tranNumber(fundingTotalUSD, 2)}</Box>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
