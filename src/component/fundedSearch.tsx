import React, { useState, useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import logo from "./logo.png";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useFindCompanyListQuery } from "../generated/graphql";
import LinktoCompanyPage from "./LinktoCompanyPage";
import SearchIcon from "@material-ui/icons/Search";
import { useStyles } from "./componentStyle"
import Base64 from "base-64";

interface CompanyInfo {
  CompanyID: string;
  CompanyName: string;
}

interface FundedSearchProps {
}

export default function FundedSearch(props: FundedSearchProps) {
  const classes = useStyles();
  const [companyInfoList, setCompanyInfoList] = useState<CompanyInfo[]>([]);
  const [value] = useState<CompanyInfo | null>(null);
  const [inputValue, setInputValue] = useState("");
  const [findCompanyListQueryResult] = useFindCompanyListQuery({
    variables: {
      name: inputValue,
    },
    pause: false,
  });

  useEffect(() => {
    let res =
      findCompanyListQueryResult.data?.QueryCompanyListByCompanyName
        ?.companyInfo;
    let newCompanyNameList: CompanyInfo[] = [];
    res?.map((value) => {
      newCompanyNameList.push({
        CompanyID: value?.companyID as string,
        CompanyName: value?.companyName as string,
      });
      return value?.companyName;
    });
    setTimeout(() => {
      setCompanyInfoList(newCompanyNameList as CompanyInfo[]);
    }, 0);
  }, [findCompanyListQueryResult]);

  function handleSearchInputChange(value: string) {
    setTimeout(() => {
      setInputValue(value);
    }, 0);
  }

  function handleSearchButtonClick() {
    let res =
      findCompanyListQueryResult.data?.QueryCompanyListByCompanyName
        ?.companyInfo;
    let id = ""
    res?.map((value) => {
      if (inputValue === value?.companyName) {
        id = value.companyID as string
      }
      return value?.companyName;
    });
    if (id === "") {
      alert("没有该公司")
      return
    }
    id = Base64.encode(id)
    window.location.href = "/CompanyPage/" + id
  }

  return (
    <div className={classes.root} style={{ boxShadow: "0 4px 0 #F8F8F8" }}>
      <Grid item xs={12} className={classes.container_searchs}>
        <Grid container>
          <Grid item xs={2}>
            <img alt="" className={classes.img_logo} src={logo} />
          </Grid>
          <Grid item xs={4}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <Autocomplete
              id="custom-input-demo"
              getOptionLabel={(option) =>
                typeof option === "string" ? option : option.CompanyName
              }
              filterOptions={(x) => x}
              options={companyInfoList}
              autoComplete
              includeInputInList
              filterSelectedOptions
              value={value}
              onInputChange={(event, newInputValue) => {
                handleSearchInputChange(newInputValue);
              }}
              renderInput={(params) => (
                <div ref={params.InputProps.ref}>
                  <input
                    id="searchInput"
                    style={{
                      width: "100%",
                      height: "36px",
                      borderRadius: "50px 50px 50px 50px",
                      outline: "none",
                      boxShadow: "none",
                      border: "2px solid #c4c7ce",
                      background: "#fff",
                      color: "#222",
                      margin: 0,
                      fontWeight: 400,
                      paddingLeft: "40px",
                      marginTop: "14px",
                    }}
                    type="text"
                    placeholder="搜索企业"
                    onKeyUp={(e) => {
                      if (e.keyCode === 13) {
                        handleSearchButtonClick();
                      }
                    }}
                    {...params.inputProps}
                  />
                </div>
              )}
              renderOption={(option) => {
                return (
                  <Grid
                    container
                    alignItems="center"
                    className={classes.searchList}
                  >
                    <LinktoCompanyPage
                      companyID={option.CompanyID}
                      companyName={option.CompanyName}
                    ></LinktoCompanyPage>
                  </Grid>
                );
              }}
            />
          </Grid>
          <Grid item xs={6}></Grid>
        </Grid>
      </Grid>
    </div>
  );
}
