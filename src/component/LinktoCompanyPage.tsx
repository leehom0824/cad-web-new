import React, { useEffect, useState } from "react";
import { NavLink as Link } from 'react-router-dom'
import Base64 from "base-64";
import { useFindCompanyOverviewQuery } from "../generated/graphql";

interface LinktoCompanyPageProps {
    companyID: string;
    companyName: string;
}

export default function LinktoCompanyPage(props: LinktoCompanyPageProps) {
    let obj = {
        pathname: "/CompanyPage/" + Base64.encode(props.companyID),
    }
    const [isExist, setIsExist] = useState(true)
    const [findCompanyOverviewQueryResult] = useFindCompanyOverviewQuery({
        variables: {
            id:  props.companyID,
        },
    });

    useEffect(() => {
        let res =
            findCompanyOverviewQueryResult.data?.QueryCompanyOverviewByCompanyID;

        if (res === null) {
            setIsExist(false)
        }
    }, [findCompanyOverviewQueryResult]);

    function handle() {
        if (isExist) {
            return (<div style={{ display: "inline", marginRight: 10 }} >
                <Link target="_blank" to={obj} style={{ textDecoration: "none", color: "#3672EC" }}>{props.companyName}</Link>
                </div>)
        } else {
            return (<div style={{ display: "inline", marginRight: 10 }} >
                {props.companyName}
                </div>)
        }
    }
    return handle()
}
