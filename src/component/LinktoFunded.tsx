import React from "react";
import { NavLink as Link } from 'react-router-dom'

interface LinktoFundedProps {
  companyID: string;
  fundingRoundType: string;
  fundedAt: string;
  raisedAmountUsd: number;
}

export default function LinktoFunded(props: LinktoFundedProps) {
  let params = props.companyID + "/" + props.fundingRoundType + "/" + props.fundedAt + "/" + props.raisedAmountUsd
  let obj = {
    pathname: "/FundedPage/" + params,
  }
  return (
    <div>
      <Link to={obj} style={{ textDecoration: "none", color: "#3672EC" }}>{props.fundedAt}</Link>
    </div>
  );
}
