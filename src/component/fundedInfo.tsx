import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import { useStyles } from "./componentStyle";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import Pagination from "@material-ui/lab/Pagination";
import Paper from "@material-ui/core/Paper";
import { useFindFinancingListQuery } from "../generated/graphql";
import Base64 from "base-64";
import LinktoFundedType from "./LinktoFundedType";
import LinktoCompanyPage from "./LinktoCompanyPage";
import { useFindCompanyOverviewQuery } from "../generated/graphql";

interface FundedInfoProps {
  id: string;
}

interface FundedInfo {
  financing_type: string;
  funded_at: string;
  total_amount: number;
  funded_companyID: string;
  investor: Investor[];
}

interface Investor {
  companyID: string;
  companyName: string;
  categoryCode: string;
}

function createData(
  financing_type: string,
  funded_at: string,
  total_amount: number,
  funded_companyID: string,
  investor: Investor[]
) {
  return {
    financing_type,
    funded_at,
    total_amount,
    funded_companyID,
    investor,
  };
}

function tranNumber(num: number | undefined, point: number) {
  if (typeof num === "undefined" || num === 0) {
    return ""
  }
  let n = num as number + ""
  let numStr = n.toString().split('.')[0]
  if (numStr.length < 5) {
    console.log(numStr);
    return numStr;
  } else if (numStr.length >= 5 && numStr.length <= 8) {
    let decimal = numStr.substring(numStr.length - 4, numStr.length - 4 + point)
    return Math.floor(num / 10000) + '.' + decimal + '万'
  } else if (numStr.length > 8) {
    let decimal = numStr.substring(numStr.length - 8, numStr.length - 8 + point);
    return Math.floor(num / 100000000) + '.' + decimal + '亿'
  }
}

export default function FundedInfo(props: FundedInfoProps) {
  const classes = useStyles();
  const [rows, setRows] = useState<FundedInfo[]>([]);
  const [newAfter, setNewAfter] = useState(0);
  const [page, setPage] = React.useState(1);
  const [isExist, setIsExist] = useState(true)
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };
  const [totalPageCount, setTotalPageCount] = useState(0);
  const [
    findFinancingListQueryResult,
    executeFindFinancingListQuery,
  ] = useFindFinancingListQuery({
    variables: {
      first: 10,
      after: newAfter,
      companyID: Base64.decode(props.id),
    },
    pause: false,
  });
  const [findCompanyOverviewQueryResult] = useFindCompanyOverviewQuery({
    variables: {
      id: Base64.decode(props.id),
    },
  });
  const [fundingTotalUSD, setFundingTotalUSD] = useState(0);
  useEffect(() => {
    let res =
      findCompanyOverviewQueryResult.data?.QueryCompanyOverviewByCompanyID;
    setFundingTotalUSD(res?.fundingTotalUSD as number);
  }, [findCompanyOverviewQueryResult]);

  useEffect(() => {
    setTimeout(() => {
      setNewAfter((page - 1) * 10);
    }, 0);
  }, [page]);

  useEffect(() => {
    let nodes =
      findFinancingListQueryResult.data?.QueryFinancingListByCompanyID?.nodes;
    let res = nodes?.map((value) => {
      let investors: Investor[] = [];
      value.investors.map((investor) => {
        investors.push(investor as Investor);
        return investor
      });
      let newFundedInfo = createData(
        value.financingInfo.fundingRoundType as string,
        value.financingInfo.fundedAt as string,
        value.financingInfo.raisedAmountUsd as number,
        value.financingInfo.companyID as string,
        investors
      );
      return newFundedInfo;
    });



    while (res?.length as number < 5) {
      res?.push(createData("", "", 0, "", []))
    }

    while (res?.length as number > 5 && res?.length as number < 10) {
      res?.push(createData("", "", 0, "", []))
    }

    setRows(res as FundedInfo[]);

    let r =
      findFinancingListQueryResult.data?.QueryFinancingListByCompanyID;

    if (r === null) {
      setIsExist(false)
    }

    let totalCount =
      findFinancingListQueryResult.data?.QueryFinancingListByCompanyID
        ?.totalCount;

    setTotalPageCount(Math.ceil(totalCount / 10));

  }, [findFinancingListQueryResult]);

  return (
    <div>
      <Grid item xs={12} className={isExist === true ? classes.container_financing : classes.noDisplay}>
        <Grid item xs={12} className={classes.financing}>
          <Box className={classes.financing_title}>融资交易</Box>
          <Box
            className={classes.financing_total_amount}
            display="flex"
            flexDirection="row"
          >
            <Box className={classes.amount1}>累计总金额</Box>
            <Box className={classes.amount2}>{tranNumber(fundingTotalUSD, 2)}</Box>
          </Box>
          <TableContainer component={Paper} style={{ boxShadow: "none" }}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead style={{ background: "#FAFAFA" }}>
                <TableRow>
                  <TableCell className={classes.financing_type_style}>
                    融资类型
                  </TableCell>
                  <TableCell className={classes.funded_at_style} align="left">
                    日期
                  </TableCell>
                  <TableCell
                    className={classes.total_amount_style}
                    align="right"
                  >
                    融资金额
                  </TableCell>
                  <TableCell align="left">投资方</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows?.map((row, index) => (
                  <TableRow key={row.total_amount}>
                    <TableCell
                      className={classes.financing_type_style}
                      component="th"
                      scope="row"
                    >
                      <LinktoFundedType
                        companyID={Base64.encode(row.funded_companyID)}
                        fundingRoundType={row.financing_type as string}
                        fundedAt={row.funded_at as string}
                        raisedAmountUsd={row.total_amount as number}
                      ></LinktoFundedType>
                    </TableCell>
                    <TableCell className={classes.funded_at_style} align="left">
                      {row.funded_at}
                    </TableCell>
                    <TableCell
                      className={classes.total_amount_style}
                      align="right"
                    >
                      {tranNumber(row.total_amount, 2)}
                    </TableCell>
                    <TableCell align="left">
                      {row.investor.map((value, i) => (
                        <LinktoCompanyPage
                          key={i}
                          companyID={value.companyID}
                          companyName={value.companyName}
                        ></LinktoCompanyPage>
                      ))}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <Box className={classes.pagination}>
              <Pagination
                count={totalPageCount}
                page={page}
                shape="rounded"
                onChange={handleChange}
                siblingCount={1}
                boundaryCount={1}
              />
            </Box>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}
